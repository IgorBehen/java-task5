package electricalAppliancesMVC;

import electricalAppliancesMVC.controller.appliance_manager.ApplianceManager;
import electricalAppliancesMVC.view.menu.Menu;

public class Main {
    public static void main(String[] args) {
        ApplianceManager am = new ApplianceManager();
        am.initData();
        Menu.menu();
    }
}
